#pragma once

#ifdef NOT_DEVICE
    #include <string.h>
    #include <stdint.h>
    #define PROGMEM
    #define memcpy_P memcpy
    static const uint16_t MAX_STREAM_BLOCK_SIZE = 1024;
    static const uint16_t INPUT_BUFFER_SIZE = 1024;
    extern "C" { const unsigned char *pgm_read_word(const unsigned char * const *memloc); }
#else
    #include <avr/pgmspace.h>
    static const uint16_t MAX_STREAM_BLOCK_SIZE = 64;
    static const uint16_t INPUT_BUFFER_SIZE = 128;
#endif

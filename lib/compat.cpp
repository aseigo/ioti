#include "compat.h"

#ifdef NOT_DEVICE
extern "C" {
const unsigned char *pgm_read_word(const unsigned char * const *memloc) { return *memloc; }
}
#endif

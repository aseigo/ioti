/*
 *   Copyright (C) 2016 Aaron Seigo <aseigo@exote.ch>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#pragma once

#include <stdint.h>
#include "compat.h"

namespace Ioti
{

class Transport;
class Protocol;

typedef void (*programFuncPtr)(const char *data, uint32_t size, uint32_t packageId, Protocol *protocol);

class Protocol
{
public:
    enum State {
        ErrorState = 0,
        NextCommandState,
        BeginSessionState,
        PackageRequestState,
        RespondProgramPassthroughState,
        ProgramPassthroughState,
        LastState
    };

    Protocol(Transport *transport, programFuncPtr program);
    ~Protocol();

    State state() const { return m_state; }
    uint32_t numberExpectedBytes() { return m_maxBytes; }

    bool recv(const char *data, uint32_t size);
    void send(const void *data, uint32_t size);
    void exitProgramState();

private:
    void sendPackage(uint32_t packageId);
    State stateForCommand(uint32_t commandId);
    uint32_t sizeForCommand(uint32_t commandId);
    void resetBuffer();
    void buffer(const char *data, uint32_t size, uint32_t bufferSize);
    const char *processIncoming(const char *data, uint32_t size, uint32_t neededDataSize);

    Transport *m_transport;
    uint32_t m_maxBytes = sizeof(uint32_t);
    State m_state = NextCommandState;
    uint32_t m_bufferSize = 0;
    uint32_t m_bufferedData = 0;
    uint32_t m_packageId = 0;
    char m_buffer[INPUT_BUFFER_SIZE];

    programFuncPtr m_program;
};

} // namespace Ioti

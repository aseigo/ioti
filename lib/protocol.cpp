/*
 *   Copyright (C) 2016 Aaron Seigo <aseigo@exote.ch>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 */
#include "protocol.h"

#include "blobs.h"
#include "compat.h"
#include "ioti.h"
#include "transport.h"

namespace Ioti
{

Protocol::Protocol(Transport *transport, programFuncPtr program)
    : m_transport(transport),
      m_program(program)
{
}

Protocol::~Protocol()
{
}

bool Protocol::recv(const char *data, uint32_t size)
{
    if (m_maxBytes > 0 && size > m_maxBytes) {
        return false;
    }

    if (m_maxBytes > 0) {
        m_maxBytes -= size;
    }

    //TODO: should protect again sizes smaller than uint32_t for those branches
    switch (m_state) {
        case NextCommandState: {
            if (size == sizeof(uint32_t)) {
                uint32_t commandId;
                memcpy(&commandId, data, sizeof(commandId));

                m_state = stateForCommand(commandId);
                m_maxBytes = sizeForCommand(commandId);
            }
            break;
        }

        case BeginSessionState: {
            if (size == sizeof(uint32_t)) {
                uint32_t protocolVersion;
                memcpy(&protocolVersion, data, sizeof(protocolVersion));
                if (protocolVersion > IOTI_CURRENT_PROTOCOL_VERSION) {
                    m_state = ErrorState;
                } else {
                    const ioti_init_data &device_init_data = m_transport->initData();
                    send(&device_init_data, sizeof(device_init_data));
                    m_state = NextCommandState;
                }
            }
            break;
        }

        case PackageRequestState: {
           const char *dataReady = processIncoming(data, size, sizeForCommand(ioti_request_package_command_id));
           if (dataReady != nullptr) {
               ioti_request_package request;

               memcpy(&request.response_id, dataReady, sizeof(request.response_id));
               dataReady += sizeof(request.response_id);

               memcpy(&request.package_id, dataReady, sizeof(request.package_id));
               dataReady += sizeof(request.package_id);

               send(&request.response_id, sizeof(request.response_id));
               sendPackage(request.package_id);

               m_packageId = request.package_id;
               m_state = NextCommandState;
           }

           break;
        }

        case RespondProgramPassthroughState: {
            if (size == sizeof(uint32_t)) {
                uint32_t requestId = *data;
                send(&requestId, sizeof(requestId));
                uint32_t zero = 0;
                send(&zero, sizeof(zero));

                m_state = ProgramPassthroughState;
                m_maxBytes = 0;
            }
            break;
        }

        case ProgramPassthroughState: {
            (*m_program)(data, size, m_packageId, this);
        }

        default:
            break;
    }

    if (m_state == NextCommandState) {
        resetBuffer();
        m_maxBytes = 4;
    }

    //printf("State is %i and max bytes is %u\n", m_state, m_maxBytes);
    return m_state != ErrorState;
}

void Protocol::sendPackage(uint32_t packageId)
{
    uint32_t size = 0;
    if (packageId < ioti_num_packages) {
        size = *ioti_package_sizes[packageId];
    }

    send(&size, sizeof(size));
    if (size < 1) {
        // if we have no size, we have nothing to send
        return;
    }

    //printf("We have gotten a request for the package! %i %u\n", request.package_id, size);
    const unsigned char *pkgData = (unsigned char *)pgm_read_word(&ioti_packages[packageId]);
    uint32_t curpos = 0;
    unsigned char sendBuffer[MAX_STREAM_BLOCK_SIZE]; // TODO: make this a member of the class?
    while (size > 0) {
        if (size > MAX_STREAM_BLOCK_SIZE) {
            memcpy_P(&sendBuffer, (unsigned char *)pkgData + curpos, MAX_STREAM_BLOCK_SIZE);
            send(&sendBuffer, MAX_STREAM_BLOCK_SIZE);
            size -= MAX_STREAM_BLOCK_SIZE;
            curpos += MAX_STREAM_BLOCK_SIZE;
        } else {
            memcpy_P(&sendBuffer, (unsigned char *)pkgData + curpos, size);
            send(&sendBuffer, size);
            size = 0;
        }
    }

}

void Protocol::send(const void *data, uint32_t size)
{
    m_transport->send(reinterpret_cast<const char *>(data), size);
}

void Protocol::exitProgramState()
{
    if (m_state == ProgramPassthroughState) {
        m_state = NextCommandState;
    }
}

Protocol::State Protocol::stateForCommand(uint32_t commandId)
{
    //printf("State of command %i, is it %i?\n", commandId, ioti_request_package_command_id);
    if (commandId == ioti_begin_session_command_id) {
        return BeginSessionState;
        return BeginSessionState;
    } else if (commandId == ioti_request_package_command_id) {
        return PackageRequestState;
    } else if (commandId == ioti_start_program_data_command_id) {
        return RespondProgramPassthroughState;
    } else if (commandId == ioti_end_session_command_id) {
        //TODO: this is not really an error, should return something less potentially confusing
        return ErrorState;
    }

    return ErrorState;
}

uint32_t Protocol::sizeForCommand(uint32_t commandId)
{
    if (commandId == ioti_begin_session_command_id) {
        return sizeof(uint32_t);
    } else if (commandId == ioti_request_package_command_id) {
        return sizeof(uint32_t) * 2;
    } else if (commandId == ioti_start_program_data_command_id) {
        return sizeof(uint32_t);
    } else if (commandId == ioti_end_session_command_id) {
        return 0;
    }

    return ErrorState;
}

void Protocol::resetBuffer()
{
    m_bufferSize = 0;
    m_bufferedData = 0;
}

void Protocol::buffer(const char *data, uint32_t size, uint32_t bufferSize)
{
    if (m_bufferSize != bufferSize) {
        if (bufferSize > INPUT_BUFFER_SIZE) {
            m_bufferSize = INPUT_BUFFER_SIZE;
        } else {
            m_bufferSize = bufferSize;
        }

        //memset(m_buffer, '\0', m_bufferSize); TODO: should this be zero'd out, or can we trust the code?
        m_bufferedData = 0;
    }

    if (m_bufferedData + size > m_bufferSize) {
        size = m_bufferSize - m_bufferedData;
    }

    memcpy(m_buffer + m_bufferedData, data, size);
}

const char *Protocol::processIncoming(const char *data, uint32_t size, uint32_t neededDataSize)
{
    if (size + m_bufferedData < sizeForCommand(ioti_request_package_command_id)) {
        buffer(data, size, neededDataSize);
        return nullptr;
    } else if (m_buffer) {
        buffer(data, size, neededDataSize);
        return m_buffer;
    } else {
        return data;
    }
}

} // namespace Ioti

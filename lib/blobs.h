#pragma once

#ifndef FORDEVICE
     #define PROGMEM
#endif

#include <stdint.h>
extern const uint32_t ioti_num_packages;
extern const unsigned char * const ioti_packages[] PROGMEM;
extern const uint32_t * const ioti_package_sizes[];


#pragma once

#include <string.h>
#include <stdint.h>

#define IOTI_CURRENT_PROTOCOL_VERSION 1

enum ioti_command_ids {
    ioti_begin_session_command_id = 1,
    ioti_request_package_command_id = 10,
    ioti_authorize_command_id = 20,
    ioti_start_program_data_command_id = 30,
    ioti_end_session_command_id = 0xFF
};

struct ioti_begin_session
{
    const uint32_t command_id = ioti_begin_session_command_id;
    uint32_t protocol_version_id = IOTI_CURRENT_PROTOCOL_VERSION;
};

struct ioti_init_data
{
    uint32_t protocol_version_id;
    uint32_t vendor_id;
    uint32_t product_id;
    uint32_t revision_id;
    uint32_t device_id;
};

struct ioti_request_package
{
    const uint32_t command_id = ioti_request_package_command_id;
    uint32_t response_id;
    uint32_t package_id;
};

struct ioti_package_metadata
{
    uint32_t response_id;
    uint32_t length;
};

struct ioti_start_program_comm
{
    const uint32_t command_id = ioti_start_program_data_command_id;
    uint32_t response_id;
};

struct ioti_start_program_comm_response
{
    uint32_t response_id;
    uint32_t success;
};

static const char *ioti_localSocketPath = "/tmp/iot_socket";


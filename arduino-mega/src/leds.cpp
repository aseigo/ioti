/*
 * Copyright (C) 2016 Aaron Seigo <aseigo@exote.ch>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#include "leds.h"

#include "Arduino.h"

#include "pinmappings.h"

void LEDs::init()
{
    pinMode(redLEDPin, OUTPUT);
    pinMode(grnLEDPin, OUTPUT);
    pinMode(bluLEDPin, OUTPUT);
}

void LEDs::set(int ledsOn)
{
    digitalWrite(redLEDPin, ledsOn & Red ? HIGH : LOW);
    digitalWrite(grnLEDPin, ledsOn & Green ? HIGH : LOW);
    digitalWrite(bluLEDPin, ledsOn & Blue ? HIGH : LOW);
    delay(50); //TODO HAAAAACK :) makes 'em visible at least a bit
}

void LEDs::runTestPattern()
{
    set(Red);
    delay(250);

    set(Green);
    delay(250);

    set(Blue);
    delay(250);

    set(None);
}

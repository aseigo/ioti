/*
 * Copyright (C) 2016 Aaron Seigo <aseigo@exote.ch>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#pragma once

#include "ioti.h"

// Enums
enum States {
              NoState = 0,
              WaitingState,
              InitState,
              ConnectedState,
            };

// Constants
static const int s_inputBufferSize = 128;
static const int s_commandBufferSize = 50;
static const ioti_init_data s_deviceInitData =
{
    .protocol_version_id = IOTI_CURRENT_PROTOCOL_VERSION,
    .vendor_id = 0x01,
    .product_id = 0x02,
    .revision_id = 0x02,
    .device_id = 0x01
};


// Global state variables and their mutators
extern char inputBuffer[];
extern char *bufferPtr;

extern bool stringMode;
extern char commandBuffer[];
extern uint16_t curCommandBufferPos;

extern States state;

void resetBufferPtr();
int inputBufferDataLength();

extern States state;

void setState(States newState);

#include "globals.h"
#include "leds.h"

char inputBuffer[s_inputBufferSize] = "\0";
char *bufferPtr = inputBuffer;

bool stringMode = true;
char commandBuffer[s_commandBufferSize];
uint16_t curCommandBufferPos = 0;

States state = NoState;

void setState(States newState)
{
    if (state == newState) {
        return;
    }

    state = newState;
    switch (state) {
        case WaitingState:
            LEDs::set(LEDs::Red);
            break;
        case InitState:
            LEDs::set(LEDs::Blue);
            break;
        case ConnectedState:
            LEDs::set(LEDs::Green);
            break;
        default:
            LEDs::set(LEDs::None);
            break;
    }
}

void resetBufferPtr()
{
    bufferPtr = inputBuffer;
    *bufferPtr = '\0';
}

int inputBufferDataLength()
{
    return bufferPtr - inputBuffer;
}

/*
 *   Copyright (C) 2016 Aaron Seigo <aseigo@exote.ch>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#include "serialtransport.h"
#include "globals.h"

SerialTransport::SerialTransport(HardwareSerial &serial, Ioti::programFuncPtr program)
    : Ioti::Transport(program),
      m_serial(serial)
{
}

SerialTransport::~SerialTransport()
{
}

const ioti_init_data &SerialTransport::initData() const
{
    return s_deviceInitData;
}

void SerialTransport::send(const char *data, uint32_t size)
{
    m_serial.write(data, size);
}

uint32_t SerialTransport::recv(const char *data, uint32_t size)
{
    const uint32_t expected = m_protocol.numberExpectedBytes();
    uint32_t rv = 0;

    if (expected == 0) {
        if (!m_protocol.recv(data, size)) {
            setState(WaitingState);
        }
        rv = size; //FIXME: BUG! the buffer may not be totally consumed!
    } else if (expected <= size) {
        if (!m_protocol.recv(data, expected)) {
            setState(WaitingState);
            rv = size;
        } else {
            rv = expected;
        }
    }

    if (m_protocol.state() == Ioti::Protocol::BeginSessionState) {
        setState(InitState);
    } else if (m_protocol.state() == Ioti::Protocol::PackageRequestState) {
        setState(ConnectedState);
    }

    return rv;
}

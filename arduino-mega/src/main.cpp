/*
 * Copyright (C) 2016 Aaron Seigo <aseigo@exote.ch>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#include "Arduino.h"

#include "ioti.h"

#include "globals.h"
#include "leds.h"
#include "serialtransport.h"

void programMode(const char *data, uint32_t size, uint32_t packageId, Ioti::Protocol *protocol);

void processCompletedStringCommand(const char *data, uint32_t size, uint32_t packageId, Ioti::Protocol *protocol)
{
    if (strncmp("END\r", data, size) == 0) {
        protocol->exitProgramState();
    } else if (strncmp("binmode\r", data, size) == 0) {
        stringMode = false;
    } else {
        // TODO: error out, or?
    }
}

void appendToCommandBuffer(const char *data, uint32_t size)
{
    if (curCommandBufferPos + size < s_commandBufferSize) {
        memcpy(commandBuffer + curCommandBufferPos, data, size);
    } else {
        curCommandBufferPos = s_commandBufferSize;
    }
}

void handleStringCommand(const char *data, uint32_t size, uint32_t packageId, Ioti::Protocol *protocol)
{
    const char *newline = (const char *)memchr(data, '\n', size);
    if (!newline) {
        // we don't havef a newline anywhere, so buffer this puppy
        appendToCommandBuffer(data, size);
        return;
    }

    if (curCommandBufferPos < s_commandBufferSize) {
        // the buffer was not overrun and a newline termination was received
        if (curCommandBufferPos > 0) {
            appendToCommandBuffer(data, newline - data + 1);
            if (curCommandBufferPos < s_commandBufferSize) {
                processCompletedStringCommand(commandBuffer, curCommandBufferPos + 1, packageId, protocol);
            }
        } else {
            processCompletedStringCommand(data, newline - data, packageId, protocol);
        }
    }

    curCommandBufferPos = 0;
    if ((uint32_t)(newline - data + 1) != size) {
        // we have more data to process
       programMode(newline + 1, size - (newline - data + 1), packageId, protocol);
    }
}

void handleBinaryCommand(const char *data, uint32_t size, uint32_t packageId, Ioti::Protocol *protocol)
{
    // TODO: nothing yet ... implement something interesting
    static const unsigned char exitCommandCode[] = { 0x00, 0x00, 0x00, 0x00 };
    if (memcmp(exitCommandCode, data, size > 4 ? 4 : size) == 0) {
        stringMode = true;
        if (size > 4) {
            programMode(data + 4, size - 4, packageId, protocol);
        }
    }
}

void programMode(const char *data, uint32_t size, uint32_t packageId, Ioti::Protocol *protocol)
{
    if (stringMode) {
        handleStringCommand(data, size, packageId, protocol);
    } else {
        handleBinaryCommand(data, size, packageId, protocol);
    }
}

SerialTransport transport(Serial, programMode);

void processBuffer()
{
    uint32_t curlen = inputBufferDataLength();
    while (curlen > 0) {
        uint32_t consumed = transport.recv(inputBuffer, curlen);
        if (consumed > 0) {
            if (curlen > consumed) {
                curlen = curlen - consumed;
                memmove(inputBuffer, inputBuffer + consumed, curlen);
                bufferPtr = inputBuffer + curlen;
                *bufferPtr = '\0';
            } else {
                resetBufferPtr();
                curlen = 0;
            }
        } else {
            break;
        }
    }
}

void setup()
{
    resetBufferPtr();
    LEDs::init();
    //LEDs::runTestPattern();
    setState(WaitingState);
    Serial.begin(115200);
}

void loop()
{
    if (inputBufferDataLength() >= s_inputBufferSize) {
        //TODO: should count how many failures and reset the session if we can't
        //      unstick the buffer
        delay(100); // give us a chance to catch up?
        processBuffer();
    } else {
        while (Serial.available()) {
            char incoming = Serial.read();
            if (incoming == -1) {
                continue;
            }

            *bufferPtr = incoming;
            *(++bufferPtr) = '\0';
            processBuffer();
            if (inputBufferDataLength() >= s_inputBufferSize) {
                break;
            }
        }
    }
}

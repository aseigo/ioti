import QtQuick 2.0

Column {
    id: root

    height: 400
    width: 500
    anchors.fill: parent

    Light {
        height: parent.height / 2
        width: parent.width
    }

    Light {
        height: parent.height / 2
        width: parent.width
    }

    Connections {
        target: device
        onRecvd: console.info("received: " + data);
        onBytesRecvd: console.info("received bytes: " + data);
    }
}

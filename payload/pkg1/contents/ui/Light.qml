import QtQuick 2.0

Rectangle {
    color: "green"

    property bool on: true

    MouseArea {
        anchors.fill: parent
        onClicked: {
            parent.on = !parent.on;
            if (parent.on) {
                parent.color = 'green';
            } else {
                parent.color = 'red';
            }

            device.sendString(parent.color + "\r\n");
            if (device.StringMode) {
                device.sendString("binmode\r\n");
                device.StringMode = false;
            }
        }
    }
}

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <zip.h>

void recursive_add_to_zip(struct zip *zipfile, char *path, int base_path_len)
{
    struct dirent *entry;
    struct stat entry_stat;
    char filepath[PATH_MAX];
    strcpy(filepath, path);
    const int path_len = strlen(filepath);

    DIR *dir = opendir(path);
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        strcpy(filepath + path_len, entry->d_name);
        if (stat(filepath, &entry_stat) == -1) {
            continue;
        }

        if (S_ISDIR(entry_stat.st_mode)) {
            const int curlen = strlen(filepath);
            filepath[curlen] = '/';
            filepath[curlen + 1] = '\0';
            recursive_add_to_zip(zipfile, filepath, base_path_len);
        } else if (S_ISREG(entry_stat.st_mode)) {
            struct zip_source *source = zip_source_file(zipfile, filepath, 0, -1);
            if (source == NULL) {
                continue;
            }

            if (zip_file_add(zipfile, filepath + base_path_len, source, ZIP_FL_OVERWRITE | ZIP_FL_ENC_UTF_8) < 0) {
                zip_source_free(source);
            }
        }
        filepath[path_len] = '\0';
    }
    closedir(dir);
}

void print_file_as_buffer(const char *path, int package_number, FILE *output)
{
    const int fd = open(path, O_RDONLY);
    const int buffer_size = 1024;
    unsigned char buffer[buffer_size];
    ssize_t bytes;
    ssize_t total_bytes = 0;
    const char *indent = "    ";

    fprintf(output, "const unsigned char package_%i[] PROGMEM = {\n%s", package_number, indent);

    while ((bytes = read(fd, buffer, buffer_size)) > 0) {
        int i = 0;
        while (i < bytes) {
            if (total_bytes > 0) {
                // comma separate the values, but of course not the first one!
                fprintf(output, ", ");
                if (total_bytes % 12 == 0) {
                    // respect 80 char columns ... because reasoons.
                    // who needs the 21st century?
                    fprintf(output, "\n%s", indent);
                }
            }

            fprintf(output, "0x%02x", buffer[i]);
            ++i;
            ++total_bytes;
        }
    }

    fprintf(output, "\n};\n");
    fprintf(output, "const uint32_t package_%i_size = %i;\n\n", (int)package_number, (int)total_bytes);
    close(fd);
}

int main(int argc, const char *argv[]) {
    if (argc == 1) {
        printf("Please provide a directory to traverse for packages as the first argument.\n");
        return 1;
    }

    DIR *dir = opendir(argv[1]);
    if (dir == NULL) {
        printf("Not a directory I can open: %s\n", argv[1]);
        return 1;
    }

    char output_path_c[PATH_MAX];
    char output_path_h[PATH_MAX];
    int output_basepath_offset = 0;
    if (argc > 2) {
        output_basepath_offset = strlen(argv[2]);
        if (output_basepath_offset < PATH_MAX - strlen("/blobs.c")) {
            strcpy(output_path_c, argv[2]);
            output_path_c[output_basepath_offset] = '/';
            output_path_c[output_basepath_offset + 1] = '\0';
            strcpy(output_path_h, output_path_c);
        } else {
            output_basepath_offset = 0;
        }
    }
    strcpy(output_path_c + output_basepath_offset, "blobs.c");
    strcpy(output_path_h + output_basepath_offset, "blobs.h");

    FILE *coutput_fd = fopen(output_path_c, "w");
    FILE *houtput_fd = fopen(output_path_h, "w");

    if (coutput_fd == NULL || houtput_fd == NULL) {
        printf("Could not open output files. Exiting\n");
        return 1;
    }

    fprintf(coutput_fd, "#include \"blobs.h\"\n\n");

    struct dirent *entry;
    struct stat entry_stat;
    char filepath[PATH_MAX];

    const char *metadata_filename = "metadata.desktop";
    const int metadata_len = strlen(metadata_filename);
    realpath(argv[1], filepath);
    const int basepath_len = strlen(filepath) + 1;
    filepath[basepath_len - 1] = '/';

    uint32_t num_packages = 0;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_name[0] == '.') {
            // ignore hidden and special dirs
            continue;
        }

        const int entry_len = strlen(entry->d_name);
        strcpy(filepath + basepath_len, entry->d_name);
        filepath[basepath_len + entry_len] = '\0';

        if (stat(filepath, &entry_stat) != -1) {
            if (!S_ISDIR(entry_stat.st_mode)) {
                continue;
            }

            // now lets check for a metadata file
            struct stat package_stat;
            filepath[basepath_len + entry_len] = '/';
            strcpy(filepath + basepath_len + entry_len + 1, metadata_filename);
            filepath[basepath_len + basepath_len + metadata_len + 2] = '\0';
            if (stat(filepath, &package_stat) == -1 ||
                !S_ISREG(package_stat.st_mode)) {
                // Wah wah .. no metadata file
                continue;
            }

            // get our path back down to just the directory
            filepath[basepath_len + entry_len + 1] = '\0';
            //printf("Got: %s\n", filepath);

            char tmpfile_template[] = "zipfile_XXXXXX";
            int fd = mkstemp(tmpfile_template);
            if (fd == -1) {
                printf("Failed to create temp file...");
                return 1;
            }

            struct zip *zipfile = zip_open(tmpfile_template, ZIP_CREATE | ZIP_TRUNCATE, NULL);
            if (zipfile == NULL) {
                printf("Failed to create zip file...");
                return 1;
            }

            recursive_add_to_zip(zipfile, filepath, strlen(filepath));

            zip_close(zipfile);
            close(fd);
            print_file_as_buffer(tmpfile_template, num_packages, coutput_fd);
            unlink(tmpfile_template);
            ++num_packages;
        }
    }

    closedir(dir);

    int i;
    fprintf(coutput_fd, "uint32_t const ioti_num_packages = %i;\n", num_packages);

    fprintf(coutput_fd, "const unsigned char * const ioti_packages[%i] PROGMEM = { ", num_packages);
    for (i = 0; i < num_packages; ++i) {
        if (i > 0) {
            fprintf(coutput_fd, ", ");
        }
        fprintf(coutput_fd, "package_%i", i);
    }
    fprintf(coutput_fd, " };\n");

    fprintf(coutput_fd, "const uint32_t * const ioti_package_sizes[%i] = { ", num_packages);
    for (i = 0; i < num_packages; ++i) {
        if (i > 0) {
            fprintf(coutput_fd, ", ");
        }
        fprintf(coutput_fd, "&package_%i_size", i);
    }
    fprintf(coutput_fd, " };\n");

    fprintf(houtput_fd, "#pragma once\n\n");
    fprintf(houtput_fd, "#ifndef FORDEVICE\n     #define PROGMEM\n#endif\n\n");
    fprintf(houtput_fd, "#include <stdint.h>\n");
    fprintf(houtput_fd, "extern const uint32_t ioti_num_packages;\n");
    fprintf(houtput_fd, "extern const unsigned char * const ioti_packages[] PROGMEM;\n");
    fprintf(houtput_fd, "extern const uint32_t * const ioti_package_sizes[];\n\n");

    return 0;
}


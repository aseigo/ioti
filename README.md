# IOTI

An "Internet of Things Interface" which provides a proof-of-concept implementation
of providing a "zero configuration" rich client for a IoT object that is hosted on
the object itself (well, the microcontroler in it, in any case).

One or more packages of QtQuick QML is pre-processed by the ioti_create_blobs binary
for storage on the IoT object. A more powerful device (laptop, smartphone, tablet, ...)
may then request one of the packages (by default the client requests package 0) and once
received it locally installs and executes the contents of the package. The UI is then
able to communicate with the device directly without futher configuration. Upon session
ending (explicit, or connection lost), the session is terminated.

Mechanisms for authorization and verification are possible and planned.

## QML API

From QML there are the following properties:

    * bool StringMode: when expecitng to recv strings set to true, false when expecting binary data

and the following methods:

    * exitingProgramMode() -> call when program mode is being left (object program-specific command)
    * endSession() -> ends the session with the device (does not (yet?) terminate UI)
    * fetchNewPackage(number packageId) -> if not in program mode, fetches another package and loads it
    * sendString(String data) -> sends a string
    * sendBytes(Array[number] data) -> sends bytes; values in the array are clamped to 0-255

and the following signals:
    * recvd(String data) -> emitted when a string is received
    * bytesRecvd(Array[int] data) -> an array of values delivered as binary data from the device (values 0-255)

## Readying the QML Packages

QML packages must be processed first before being added to an ioti object. This is done using the ioti_create_blobs
binary. It takes one or two parameters: the first is the path to a directory containing QML packages (in
uncompressed KPackage format), and the optional second parameter is a path to where to place the output. The
output is two files: blobs.h and blobs.cpp. You can find examples of these files already in the lib/ directory,
which were generated from the contents of the payload/ directory.

To generate new blobs.h/cpp files from the top-level ioti, run:

    ioti_create_blobs payload lib

## Running a local ioti service

The localserver directory contains an ioti service written using Qt to simplify various tasks, such
as listening on a local port for connections. This application makes it much easier to test the ioti
code since it can be easily run on a full desktop system before creating a device image.

It can be run simply by invoking ioti_localserver and then running a client in local socket mode.

## Creating a device image

In the arduino-mega directory is a platformio.org project containing sources that can be easily
built to create an image for the arduino-mega. Just start the Atom IDE with the platformio
extensions (available from platformio.org), load the arduino-mega directory as a project directory,
and install to the device from there.

This should be easy to adapt to other devices supported by platformio, or with a bit more effort
probably most any microcontroler.

The most important files for customization purposes are:

    globals.[h|cpp] -> all the global variables are declared and instantiated here.
                       Buffer sizes can be tweaked here, which should probably be at least
                       40 bytes in size.
    pinmappings.h -> where all pin numbers should be defined here to keep the rest of the code
                     as portable between boards as possible.
    leds.[h|cpp] -> util functions to manage LEDs used to show session status. Not critical to have.

The real business happens in the ioti lib, which is unsurprisingly found in the lib/ directory. The
Ioti::Protocol class is the core component, and it uses a Ioti::Transport subclass. There is a serial
transport implementation for Ioti::Transport included in the arduino-mega sources.

## Using the Client

The client is built in the client/ directory as ioti_client. If run with no commands, it will attempt
to connecto to an ioti server using a local socket (the -l option does the same). 

If run with the -s option it will attempt to communicate over serial port. You can pass the name of
the serial port device to use via the -s option as well, otherwise the client will attempt to
autodetect which serial port to use. The baud rate can be set with the -b option.

The client uses KPackage to install and access the QML payloads delivered from an ioti service.
Note that you must be running a very recent version of KPackage (e.g. 5.21) for the installation
process to work properly.

## TODO

* A better demo of QML packages
* Validation of the package
    * Add a cryptographic hash to the packages
    * Check against a registry service
* Authorization of the client device
    * Allow device to send authorization required commands to client
    * Provide auth mechanisms on client side for QML to hook into
    * Provide a sample auth service for the device to access
    * Client will auth with service, provide auth token to QML, QML can use that token with commands
* More microcontollers
    * Create a strategy for the board-specific source bits, such as pinmappings, globalsh, and Ioti::Transports
      Probably platforio already has some convention for this ...
* Move all variables generated by ioti_create_blobs to progmem
* Uninstall the QML package on session completion if it was the version from the device
* There are still TODOs .. those should get done

## Diretory Layout

arduino-mega => impl for the arduino mega (C++)
lib => common code shared between applications
localserver => a local socket implementation of the device side protocol (C++)
client => the GUI application to run on the laptop/mobile (C++; Qt5; KPackage, KArchive)
util => ioti_create_blobs which creates blob.h/blob.c files give a path to a diretory containing QML packages

### Coming soon ..?
authorization => the server application that demonstrates possibilities for authentication of clients and device packages (Elixir?)

## Use case ideas

### Industrial / Commercial
* C&C status panel
* 3D printer controller
* Train schedule live cast in the train staion
* Interactive product information: user manual included in the object!

### Home
* Security monitor status and controls
* TV / mediacenter remote controls
* Garden watering controls
* Lighting controls (timers, e.g.)
* Coffee maker status and control

### Art and Demo
* Photo-swap box
* Personal memory storage: store your memories associated with an object _in_ the object!
* The Liar: an object that presents a survey, but then changes your responses
* Digital Prayer Flag: an object that lets you put your thoughts and wishes (prayers) into it
                       and which plays them back to other objects / people on a continous
                       loop. Could be hung from a tree following tradition.
* Fortun Teller: a device that prints out 

## Device Protocol
* Start session
    <= uint32_t command_id 1
    <= uint32_t protocol version id
    => uint32_t command id 0x01
    => device id (uint32_t vendor id, uint32_t product id, 2 byte revision id, 6 byte device id)
* Fetch package
    <= uint32_t commmand id 10
    <= uint32_t request id,
    <= uint32_t, index
    => uint32_t request id
    => uint32_ts length (0 == no such package)
    => stream length, zip'd package
* Authorize
    <= uint32_t command id 20
    <= uint32_t request id
    <= uint32_t length
    <= stream length auth blob
    => uint32_t request id, 1 byte 0 (success) || non-zero (fail)
* Enter application mode
    <= uint32_t command id 30
    <= uint32_t request id
    => uint32_t request id
    => uint32_t 0 (success) || non-zero (fail)
* End session
    <= uint32_t command id 0xFF

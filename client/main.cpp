/*
 * Copyright (C) 2014 Aaron Seigo <aseigo@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#include <unistd.h>

#include <QApplication>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QDebug>
#include <QQmlEngine>
#include <QQmlContext>
#include <QSerialPort>
#include <QSerialPortInfo>

#include "connection.h"
#include "view.h"

#include "../lib/ioti.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setQuitOnLastWindowClosed(false);
    app.setApplicationName("ioti_client");
    app.setApplicationVersion("0.1");

    QCommandLineParser parser;
    parser.setApplicationDescription("Internet Of Things Human Interface");
    parser.addHelpOption();
    parser.addVersionOption();

    /*** Options between different communication mechanisms **/
    QCommandLineOption socketPortPathOpt(QStringList() << "p" << "path", "Local socket or serial port");
    parser.addOption(socketPortPathOpt);

    /*** Local socket options ***/
    QCommandLineOption useLocalSocketOpt("l", "Use a local socket");
    parser.addOption(useLocalSocketOpt);

    /*** Serial port options ***/
    QCommandLineOption useSerialOpt("s", "Use serial communication");
    parser.addOption(useSerialOpt);

    QCommandLineOption serialBaudOpt(QStringList() << "b" << "serialBuad", "Baud rate", "baudrate", "115200");
    parser.addOption(serialBaudOpt);

    parser.process(app);

    Connection *connection = 0;

    if (parser.isSet(useSerialOpt)) {
        QString serialPort = parser.value(socketPortPathOpt);

        if (serialPort.isEmpty()) {
            QList<QSerialPortInfo> ports = QSerialPortInfo::availablePorts();
            qInfo() << "number of ports is" << ports.size();
            for (auto port : ports) {
                qInfo() << "port ->" << port.portName() << port.vendorIdentifier() << port.productIdentifier();
                if (port.portName().contains("ttyUSB")) {
                    serialPort = port.portName();
                    break;
                }
            }

            if (serialPort.isEmpty()) {
                qFatal("Serial port communication requested, but no port found!");
            }
        }

        qInfo() << "PORT" << serialPort;
        qint32 baudRate = parser.value(serialBaudOpt).toInt(); // yay, overflows!

        connection = new SerialConnection(serialPort, baudRate);
    } else {
        QString path = parser.value(socketPortPathOpt);
        if (path.isEmpty()) {
            path = ioti_localSocketPath;
        }
        connection = new LocalSocketConnection(path);
    }

    View view;
    view.engine()->rootContext()->setContextProperty(QLatin1String("device"), connection->deviceCommunication());
    QObject::connect(connection, &Connection::packageReady, &view, &View::setPackage);
    QObject::connect(&view, &View::closing, connection, &Connection::cleanup);
    QObject::connect(connection, &QObject::destroyed, &app, &QCoreApplication::quit);

    view.show();

    return app.exec();
}

/*
 *   Copyright (C) 2016 Aaron Seigo <aseigo@exote.ch>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#pragma once

#include <QObject>

#include <QLocalSocket>
#include <QSerialPort>
#include <QTemporaryFile>

#include <KPackage/Package>

class KJob;
class DeviceCommunication;

class Connection : public QObject
{
    Q_OBJECT

public:
    Connection(QObject *parent = 0);
    ~Connection();

    DeviceCommunication *deviceCommunication();

public Q_SLOTS:
    void sendString(const QString &data);
    void sendBytes(const QList<int> &data);
    void cleanup();

Q_SIGNALS:
    void packageReady(const KPackage::Package &package);

protected:
    QByteArray m_recvBuffer;

protected Q_SLOTS:
    void fetchNewPackage(uint32_t packageId);
    void startSession();
    void processBuffer();
    void endSession();
    void exitingProgramMode();

private Q_SLOTS:
    void packageInstallCompleted(KJob *job);

private:
    virtual void sendData(const char *data, qint64 size) = 0;
    virtual void cleanupAndExit() = 0;

    enum State {
        AgainState = 0,
        InitState,
        FetchPackageMetadataState,
        FetchPackageState,
        EnteringProgramCommunicationState,
        ProgramCommunicationState,
        IdleState
    };

    State handlerInit();
    State handlerReadPackageMetadata();
    State handlerReadPackage();
    State handlerEnterProgramCommunication();

    void sendFetchPackage(uint32_t packageId);
    void sendStartProgramMode();

    bool checkResponseId(uint32_t responseId);

    Connection();
    State m_state;
    uint32_t m_responseId = 0;
    uint32_t m_dataStreamBytesLeft;
    QTemporaryFile m_packageFile;
    KPackage::Package m_package;
    DeviceCommunication *m_deviceCommunication;

    static const QString s_packageInstallRoot;
};

class LocalSocketConnection : public Connection
{
    Q_OBJECT
public:
    LocalSocketConnection(const QString &socketPath, QObject *parent = 0);

public Q_SLOTS:
    void sendData(const char *data, qint64 size);
    void cleanupAndExit();

private Q_SLOTS:
    void connected();
    void disconnected();
    void readData();

private:
    QLocalSocket *m_localSocket;
};

class SerialConnection : public Connection
{
    Q_OBJECT
public:
    SerialConnection(const QString &portName, qint32 baudRate, QObject *parent = 0);
    ~SerialConnection();

public Q_SLOTS:
    void sendData(const char *data, qint64 size);
    void cleanupAndExit();
    void error(QSerialPort::SerialPortError error);

private Q_SLOTS:
    void readData();

private:
    QSerialPort *m_port;

    void setupPort(const QString &portName, qint32 baudRate);
    void closePort();
};

class DeviceCommunication : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool StringMode READ inStringMode WRITE setStringMode NOTIFY stringModeChanged)

public:
    DeviceCommunication(QObject *parent = 0);

    void passInBuffer(const QByteArray &array);

    bool inStringMode() const;
    void setStringMode(bool strings);

Q_SIGNALS:
    void exitingProgramMode();
    void endSession();
    void fetchNewPackage(int packageId);
    void sendString(const QString &data);
    void sendBytes(const QList<int> &data);
    void recvd(const QString &data);
    void bytesRecvd(const QList<int> &data);
    void stringModeChanged(bool inStringMode);

private:
    bool m_stringMode = true;
};


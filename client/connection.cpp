/*
 *   Copyright (C) 2016 Aaron Seigo <aseigo@exote.ch>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#include "connection.h"

#include "limits.h"

#include <QDebug>
#include <QLocalSocket>
#include <QMetaEnum>
#include <QTimer>

#include "../lib/ioti.h"
#include <iostream>

#include <KPackage/PackageLoader>

#define MAX_READ 8096

const QString Connection::s_packageInstallRoot = QString("ioti/ui/");

LocalSocketConnection::LocalSocketConnection(const QString &socketPath, QObject *parent)
    : Connection(parent),
      m_localSocket(new QLocalSocket(this))
{
    connect(m_localSocket, &QLocalSocket::connected, this, &LocalSocketConnection::connected);
    connect(m_localSocket, &QLocalSocket::disconnected, this, &LocalSocketConnection::disconnected);
    connect(m_localSocket, &QIODevice::readyRead, this, &LocalSocketConnection::readData);

    qDebug() << "connecting to" << socketPath;
    m_localSocket->connectToServer(socketPath);
}

void LocalSocketConnection::connected()
{
    qDebug() << "Connected!";
    startSession();
}

void LocalSocketConnection::disconnected()
{
    qDebug() << "Disconnected!";
}

void LocalSocketConnection::readData()
{
    QByteArray tmp = m_localSocket->read(MAX_READ);
    if (tmp.isEmpty()) {
        return;
    }

    m_recvBuffer.append(tmp);
    processBuffer();

    if (!m_recvBuffer.isEmpty()) {
        QMetaObject::invokeMethod(this, "processBuffer");
    } else if (!m_localSocket->atEnd()) {
        QMetaObject::invokeMethod(this, "readData");
    }
}

void LocalSocketConnection::sendData(const char *data, qint64 size)
{
    m_localSocket->write(data, size);
}

void LocalSocketConnection::cleanupAndExit()
{
    endSession();
    deleteLater();
}

SerialConnection::SerialConnection(const QString &portName, qint32 baudRate, QObject *parent)
    : Connection(parent),
      m_port(new QSerialPort(this))
{
    connect(m_port, &QSerialPort::readyRead, this, &SerialConnection::readData);
    connect(m_port, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(error(QSerialPort::SerialPortError)));

    setupPort(portName, baudRate);
    if (!m_port->open(QIODevice::ReadWrite)) {
        const QMetaObject *mobj = m_port->metaObject();
        const int error = m_port->error();
        QString msg("Could not open the requested serial port: " + portName + ", error: " + mobj->enumerator(mobj->indexOfEnumerator("SerialPortError")).valueToKey(error));
        qFatal(msg.toUtf8(), 0);
    }

    //FIXME: why does it need to wait exactly 1 second?
    QTimer::singleShot(1000, this, &SerialConnection::startSession);
}

SerialConnection::~SerialConnection()
{
}

void SerialConnection::closePort()
{
    if (m_port->isOpen()) {
        endSession();
        m_port->flush();
        m_port->close();
    }
}

void SerialConnection::setupPort(const QString &portName, qint32 baudRate)
{
    m_port->setPortName(portName);
    m_port->setBaudRate(baudRate);
    m_port->setDataBits(QSerialPort::Data8);
    m_port->setParity(QSerialPort::NoParity);
    m_port->setStopBits(QSerialPort::OneStop);
}

void SerialConnection::readData()
{
    QByteArray newBytes = m_port->readAll();

    if (newBytes.isEmpty()) {
        return;
    }

    m_recvBuffer += newBytes;
    processBuffer();

    if (!m_port->atEnd()) {
        QMetaObject::invokeMethod(this, "readData");
    }
}

void SerialConnection::sendData(const char *data, qint64 size)
{
    m_port->write(data, size);
}

void SerialConnection::cleanupAndExit()
{
    closePort();
    deleteLater();
}

void SerialConnection::error(QSerialPort::SerialPortError error)
{
    if (error > QSerialPort::NoError) {
        qInfo() << "SERIAL PORT ERROR!" << error;
    }
}

Connection::Connection(QObject *parent)
    : QObject(parent),
      m_state(InitState),
      m_package(KPackage::PackageLoader::self()->loadPackage("KPackage/GenericQML")),
      m_deviceCommunication(new DeviceCommunication(this))
{
    connect(m_deviceCommunication, &DeviceCommunication::sendString, this, &Connection::sendString);
    connect(m_deviceCommunication, &DeviceCommunication::sendBytes, this, &Connection::sendBytes);
    connect(m_deviceCommunication, &DeviceCommunication::endSession, this, &Connection::endSession);
    connect(m_deviceCommunication, &DeviceCommunication::exitingProgramMode, this, &Connection::exitingProgramMode);
    connect(m_deviceCommunication, &DeviceCommunication::fetchNewPackage, this, &Connection::fetchNewPackage);
}

Connection::~Connection()
{
}

DeviceCommunication *Connection::deviceCommunication()
{
    return m_deviceCommunication;
}

void Connection::startSession()
{
    ioti_begin_session command;
    sendData((const char *)&command, sizeof(command));
}

void Connection::processBuffer()
{
    State newState = AgainState;
    switch (m_state) {
        case InitState:
            newState = handlerInit();
            break;
        case FetchPackageMetadataState:
            newState = handlerReadPackageMetadata();
            break;
        case FetchPackageState:
            newState = handlerReadPackage();
            break;
        case EnteringProgramCommunicationState:
            newState = handlerEnterProgramCommunication();
            break;
        case ProgramCommunicationState:
            m_deviceCommunication->passInBuffer(m_recvBuffer);
            m_recvBuffer.clear();
            break;

        default:
            qWarning() << "Fall through state .. clearing buffer with" << m_recvBuffer.size() << "bytes in it";
            m_recvBuffer.clear();
    }

    if (newState != AgainState) {
        m_state = newState;
    }
}

Connection::State Connection::handlerInit()
{
    if ((unsigned int)m_recvBuffer.size() >= sizeof(ioti_init_data)) {
        ioti_init_data init_data;
        const char *raw = m_recvBuffer.constData();

        memcpy(&init_data.protocol_version_id, raw, sizeof(init_data.protocol_version_id));
        raw += sizeof(init_data.protocol_version_id);

        memcpy(&init_data.vendor_id, raw, sizeof(init_data.vendor_id));
        raw += sizeof(init_data.vendor_id);

        memcpy(&init_data.product_id, raw, sizeof(init_data.product_id));
        raw += sizeof(init_data.product_id);

        memcpy(&init_data.revision_id, raw, sizeof(init_data.revision_id));
        raw += sizeof(init_data.revision_id);

        memcpy(&init_data.device_id, raw, sizeof(init_data.device_id));

        qInfo() << "Initialized: " << init_data.protocol_version_id << init_data.vendor_id << init_data.product_id << init_data.revision_id << init_data.device_id;
        if (init_data.protocol_version_id != IOTI_CURRENT_PROTOCOL_VERSION) {
            qWarning() << "Got a different protocol version than expected" << init_data.protocol_version_id << "vs" << IOTI_CURRENT_PROTOCOL_VERSION;
        }

        m_recvBuffer.remove(0, sizeof(ioti_init_data));
        sendFetchPackage(0);
        return FetchPackageMetadataState;
    }

    return AgainState;
}

Connection::State Connection::handlerReadPackageMetadata()
{
    if ((unsigned int)m_recvBuffer.size() >= sizeof(ioti_package_metadata)) {
        const char *raw = m_recvBuffer.constData();
        ioti_package_metadata metadata;

        memcpy(&metadata.response_id, raw, sizeof(metadata.response_id));
        raw += sizeof(metadata.response_id);

        memcpy(&metadata.length, raw, sizeof(metadata.length));

        checkResponseId(metadata.response_id);

        m_dataStreamBytesLeft = metadata.length;
        if (m_dataStreamBytesLeft < 1) {
            qWarning() << "The requested package does not exist!";
            return IdleState;
        }

        //qInfo() << metadata.response_id << "WAITING FOR" << m_dataStreamBytesLeft << "BYTES OF PACKAGE DATA TO ARRIVE";
        m_recvBuffer.remove(0, sizeof(metadata));
        return FetchPackageState;
    }

    return AgainState;
}

Connection::State Connection::handlerReadPackage()
{
    if (m_dataStreamBytesLeft > 0) {
        //qInfo() << "Got" << m_recvBuffer.size() << "and waiting for" << m_dataStreamBytesLeft;
        if (!m_packageFile.isOpen()) {
            m_packageFile.open();
            //m_packageFile.setAutoRemove(false);
        }

        if ((uint32_t)m_recvBuffer.size() < m_dataStreamBytesLeft) {
            m_packageFile.write(m_recvBuffer);
            m_dataStreamBytesLeft -= m_recvBuffer.size();
            m_recvBuffer.clear();
        } else {
            m_packageFile.write(m_recvBuffer.constData(), m_dataStreamBytesLeft);
            m_recvBuffer.remove(0, m_dataStreamBytesLeft);
            m_dataStreamBytesLeft = 0;
            qInfo() << "Received the requested package, stored at ..." << m_packageFile.fileName() << m_package.hasValidStructure();
            m_packageFile.flush();
            KJob *installJob = m_package.update(m_packageFile.fileName(), s_packageInstallRoot);
            if (installJob) {
                connect(installJob, &KJob::result, this, &Connection::packageInstallCompleted);
            } else {
                m_packageFile.close();
            }
        }
    }

    return m_dataStreamBytesLeft > 0 ? AgainState : IdleState;
}

Connection::State Connection::handlerEnterProgramCommunication()
{
    if ((unsigned int)m_recvBuffer.size() >= sizeof(ioti_start_program_comm_response)) {
        ioti_start_program_comm_response response;
        const char *raw = m_recvBuffer.constData();

        memcpy(&response.response_id, raw, sizeof(response.response_id));
        raw += sizeof(response.response_id);

        if (!checkResponseId(response.response_id)) {
            return IdleState;
        }

        memcpy(&response.success, raw, sizeof(response.success));

        if (response.success != 0) {
            deleteLater(); // TODO maybe a little TOO brutal?
            return IdleState;
        }

        qInfo() << "In program communication state!";
        m_recvBuffer.remove(0, sizeof(ioti_start_program_comm_response));
        emit packageReady(m_package);
        return ProgramCommunicationState;
    }

    return AgainState;
}

void Connection::endSession()
{
    uint32_t endCommand = ioti_end_session_command_id;
    sendData((const char *)&endCommand, sizeof(endCommand));
}

void Connection::exitingProgramMode()
{
    m_state = IdleState;
}

void Connection::fetchNewPackage(uint32_t packageId)
{
    if (m_state == ProgramCommunicationState) {
        return;
    }

    m_state = FetchPackageMetadataState;
    sendFetchPackage(packageId);
}

void Connection::sendFetchPackage(uint32_t packageId)
{
    ioti_request_package req;
    req.response_id = ++m_responseId;
    req.package_id = packageId;

    sendData((const char *)&req.command_id, sizeof(req.command_id));
    sendData((const char *)&req.response_id, sizeof(req.response_id));
    sendData((const char *)&req.package_id, sizeof(req.package_id));
}

void Connection::sendStartProgramMode()
{
    ioti_start_program_comm req;
    req.response_id = ++m_responseId;
    sendData((const char *)&req.command_id, sizeof(req.command_id));
    sendData((const char *)&req.response_id, sizeof(req.response_id));
    m_state = EnteringProgramCommunicationState;
}

void Connection::sendString(const QString &string)
{
    QByteArray latin = string.toLatin1();
    sendData(latin.data(), latin.size());
}

void Connection::sendBytes(const QList<int> &data)
{
    QByteArray bytes;
    bytes.reserve(data.size());
    for (int integer: data) {
        // we're only intersted in the low byte
        // we use ints instead of bytes because JS has no useful concept of bytes
        // that we can use with QML/C++
        unsigned char c = static_cast<unsigned char>(integer & 0xFF);
        bytes.append(c);
    }
    sendData(bytes.data(), bytes.size());
}

void Connection::cleanup()
{
    cleanupAndExit();
}

bool Connection::checkResponseId(uint32_t responseId)
{
    if (responseId != m_responseId) {
        deleteLater();
        return false;
    }

    return true;
}

void Connection::packageInstallCompleted(KJob *job)
{
    int error = job->error();
    if (error && error != KPackage::Package::NewerVersionAlreadyInstalledError) {
        qWarning() << "Failed to set up the received package. Reason given:" << job->error() << job->errorText() << m_package.path();
        return;
    }

    sendStartProgramMode();
    m_packageFile.close();
}

DeviceCommunication::DeviceCommunication(QObject *parent)
    : QObject(parent)
{
}

bool DeviceCommunication::inStringMode() const
{
    return m_stringMode;
}

void DeviceCommunication::setStringMode(bool strings)
{
    m_stringMode = strings;
}

void DeviceCommunication::passInBuffer(const QByteArray &array)
{
    if (m_stringMode) {
        emit recvd(QString(array));
    } else {
        QByteArray::const_iterator it = array.begin();
        QList<int> horribleQmlHackCollection;
        while (it != array.end()) {
            horribleQmlHackCollection << *it;
            ++it;
        }
        emit bytesRecvd(horribleQmlHackCollection);
    }
}

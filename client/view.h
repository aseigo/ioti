/*
 *   Copyright (C) 2016 Aaron Seigo <aseigo@exote.ch>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#pragma once

#include <QQuickView>

#include <KPackage/Package>

class View : public QQuickView
{
    Q_OBJECT

public:
    View(QWindow *parent = 0);
    ~View();

public Q_SLOTS:
    void setPackage(const KPackage::Package &package);

Q_SIGNALS:
    void closing();

protected:
    void closeEvent(QCloseEvent *event);

private:
    KPackage::Package m_package;
};

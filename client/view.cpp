/*
 *   Copyright (C) 2016 Aaron Seigo <aseigo@exote.ch>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#include "view.h"

#include <QDebug>

#include <KDeclarative/KDeclarative>

View::View(QWindow *parent)
    : QQuickView(parent)
{
    KDeclarative::KDeclarative binder;
    binder.setDeclarativeEngine(engine());
    binder.setupBindings();
    connect(this, SIGNAL(closing(QQuickCloseEvent *)), this, SIGNAL(closing()));
}

View::~View()
{

}

void View::setPackage(const KPackage::Package &package)
{
    m_package = package;
    const QString mainPath = m_package.filePath("mainscript");
    qInfo() << "Package ready at:" << m_package.path() << mainPath;
    setSource(mainPath);
}


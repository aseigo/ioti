/*
 *   Copyright (C) 2016 Aaron Seigo <aseigo@exote.ch>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#include "localsockettransport.h"

#include <QLocalSocket>

#include "lib/compat.h"

static const ioti_init_data deviceInitData =
{
    protocol_version_id: IOTI_CURRENT_PROTOCOL_VERSION,
    vendor_id: 2,
    product_id: 3,
    revision_id: 4,
    device_id: 5,
};

LocalSocketTransport::LocalSocketTransport(QLocalSocket *socket, Ioti::programFuncPtr program)
    : Ioti::Transport(program),
      m_socket(socket)
{
    connect(socket, &QIODevice::readyRead, this, &LocalSocketTransport::recv);
    connect(socket, &QLocalSocket::disconnected, this, &LocalSocketTransport::disconnected);

    if (socket->bytesAvailable()) {
        recv();
    }
}

LocalSocketTransport::~LocalSocketTransport()
{
    if (m_socket) {
        m_socket->close();
        delete m_socket;
    }
}


const ioti_init_data &LocalSocketTransport::initData() const
{
    return deviceInitData;
}

void LocalSocketTransport::disconnected()
{
    m_socket->deleteLater();
    m_socket = 0;
    deleteLater();
}

void LocalSocketTransport::recv()
{
    if (!m_socket) {
        return;
    }

    uint32_t maxBytes = m_protocol.numberExpectedBytes();
    //printf("Waiting for %u bytes\n", maxBytes);
    if (maxBytes == 0) {
        //printf("bad maxBytes of %1 ...\n", maxBytes);
        maxBytes = MAX_STREAM_BLOCK_SIZE;
    }

    //printf("Prepping the buffer...\n");
    char buffer[maxBytes];
    const qint64 bytesRead = m_socket->read(buffer, maxBytes);
    //printf("Recvd %i bytes\n", bytesRead);

    if (bytesRead > 0) {
        m_protocol.recv(buffer, bytesRead);
    }

    if (m_socket->bytesAvailable()) {
        recv();
    }
}

void LocalSocketTransport::send(const char *data, unsigned int size)
{
    if (!m_socket) {
        return;
    }

    m_socket->write(data, size);
}


/*
 *   Copyright (C) 2016 Aaron Seigo <aseigo@exote.ch>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#pragma once

#include <QObject>

class QLocalSocket;

#include "lib/transport.h"

class LocalSocketTransport : public QObject, public Ioti::Transport
{
    Q_OBJECT

public:
    LocalSocketTransport(QLocalSocket *socket, Ioti::programFuncPtr program);
    ~LocalSocketTransport();

    const ioti_init_data &initData() const;
    void send(const char *data, uint32_t size);

public Q_SLOTS:
    void recv();
    void disconnected();

private:
    QLocalSocket *m_socket;
};

/*
 * Copyright (C) 2016 Aaron Seigo <aseigo@exote.ch>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#include "listener.h"

#include <iostream>
#include <iomanip>

#include <QDebug>
#include <QLocalServer>
#include <QLocalSocket>

#include "lib/ioti.h"
#include "lib/protocol.h"

#include "localsockettransport.h"

// hooorrible, but this is how to do it on the device, so .. yeah.
bool stringMode = true;
static const int commandBufferSize = 50;
char commandBuffer[commandBufferSize] = { '\0' };
uint16_t curCommandBufferPos = 0;
void programMode(const char *data, uint32_t size, uint32_t packageId, Ioti::Protocol *protocol);

void processCompletedStringCommand(const char *data, uint32_t size, uint32_t packageId, Ioti::Protocol *protocol)
{
    // echo to console
    std::cout << "Package " << packageId << ", string msg: ";
    for (uint32_t i = 0; i < size; i++)
    {
        std::cout << data[i];
    }
    std::cout << std::endl;

    // process command
    if (strncmp("END\r", data, size) == 0) {
        std::cout << "exiting program state" << std::endl;
        protocol->exitProgramState();
    } else if (strncmp("binmode\r", data, size) == 0) {
        std::cout << "entering binary mode" << std::endl;
        stringMode = false;
    } else {
        const char *send = "Roger\r\n";
        protocol->send(send, strlen(send));
    }
}

void appendToCommandBuffer(const char *data, uint32_t size)
{
    if (curCommandBufferPos + size < commandBufferSize) {
        memcpy(commandBuffer + curCommandBufferPos, data, size);
    } else {
        curCommandBufferPos = commandBufferSize;
    }
}

void handleStringCommand(const char *data, uint32_t size, uint32_t packageId, Ioti::Protocol *protocol)
{
    const char *newline = (const char *)memchr(data, '\n', size);
    if (!newline) {
        // we don't havef a newline anywhere, so buffer this puppy
        appendToCommandBuffer(data, size);
        return;
    }

    if (curCommandBufferPos < commandBufferSize) {
        // the buffer was not overrun and a newline termination was received
        if (curCommandBufferPos > 0) {
            appendToCommandBuffer(data, newline - data + 1);
            if (curCommandBufferPos < commandBufferSize) {
                processCompletedStringCommand(commandBuffer, curCommandBufferPos + 1, packageId, protocol);
            }
        } else {
            processCompletedStringCommand(data, newline - data, packageId, protocol);
        }
    }

    curCommandBufferPos = 0;
    if ((uint32_t)(newline - data + 1) != size) {
        // we have more data to process
       programMode(newline + 1, size - (newline - data + 1), packageId, protocol);
    }
}

void handleBinaryCommand(const char *data, uint32_t size, uint32_t packageId, Ioti::Protocol *protocol)
{
    // binary mode!
    static const unsigned char exitCommandCode[] = { 0x00, 0x00, 0x00, 0x00 };

    std::cout << "Package " << packageId << " with " << size << " bytes binary data: ";
    for (uint32_t i = 0; i != size; i++)
    {
        std::cout << " 0x"
                  << std::hex << std::setw(2) << std::setfill('0') << static_cast<unsigned int>(data[i]);
    }
    std::cout << std::endl;

    if (memcmp(exitCommandCode, data, size > 4 ? 4 : size) == 0) {
        std::cout << "Exiting binary mode!" << std::endl;
        stringMode = true;
        if (size > 4) {
            programMode(data + 4, size - 4, packageId, protocol);
        }
    } else {
        const unsigned char bytes[] = { 0x01, 0x02, 0x03, 0x04 };
        protocol->send(bytes, 4);
    }
}

void programMode(const char *data, uint32_t size, uint32_t packageId, Ioti::Protocol *protocol)
{
    if (stringMode) {
        handleStringCommand(data, size, packageId, protocol);
    } else {
        handleBinaryCommand(data, size, packageId, protocol);
    }
}

Listener::Listener(QObject *parent)
    : QObject(parent),
      m_server(new QLocalServer(this))
{
    connect(m_server, &QLocalServer::newConnection, this, &Listener::acceptConnection);

    const QString socketPath(ioti_localSocketPath);
    if (!m_server->listen(socketPath)) {
        m_server->removeServer(socketPath);
        if (!m_server->listen(socketPath)) {
            qDebug() << "Utter failure to start server";
            exit(-1);
        }
    }

    if (m_server->isListening()) {
        qDebug() << "Listening on " << m_server->serverName();
    }
}

Listener::~Listener()
{
}

void Listener::acceptConnection()
{
    QLocalSocket *socket = m_server->nextPendingConnection();
    qDebug() << "Accepted a connetion!";
    if (socket) {
        new LocalSocketTransport(socket, &programMode);
    }
}

